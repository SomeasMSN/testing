*** Settings ***
Library    Selenium2Library

*** Variables ***
${url}=     https://smallpdf.com/pdf-to-word

*** Keywords ***

Open URL
    Open Browser    ${url}      Chrome
    Maximize Browser Window
Upload file
    Choose File    id=__picker-input    ${CURDIR}/test_file.pdf

*** Test Cases ***
Upload
    Open URL
    Upload file



